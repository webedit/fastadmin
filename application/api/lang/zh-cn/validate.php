<?php
return [
    'Email already occupied' => '邮箱已经被占用',
    'Username already occupied' => '用户名已经被占用',
    'Nickname already occupied' => '昵称已经被占用',
    'Mobile already occupied' => '该手机号已经占用',
    'Mobile not exists' => '手机号不存在',
    'Email not exists' => '邮箱不存在',
    'Verification code is incorrect' => '验证码不正确',
];
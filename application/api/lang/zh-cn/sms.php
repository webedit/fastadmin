<?php
return [
    'Verification code is incorrect' => '验证码不正确',
    'Mobile is incorrect' => '手机号不正确',
    'Send frequently' => '发送频繁',
    'Already registered' => '已被注册',
    'Already occupied' => '已被占用',
    'Not registered' => '未注册',
    'Send successful' => '发送成功',
    'Send failed, please check the SMS config is correct' => '发送失败，请检查短信配置是否正确',
    'Successful' => '成功',
];
<?php
return [
    'Verification code is incorrect' => '验证码不正确',
    'Send frequently' => '发送频繁',
    'Already registered' => '已被注册',
    'Already occupied' => '已被占用',
    'Not registered' => '未注册',
    'Send successful' => '发送成功',
    'Send failed' => '发送失败',
    'Successful' => '成功',
];
<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\library\Ems as Emslib;
use app\common\model\User;
use think\Hook;

/**
 * 邮箱验证码接口
 */
class Ems extends Api
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 发送验证码
     *
     * @ApiMethod (POST)
     * @param string $email 邮箱
     * @param string $event 事件名称,register,changeemail,resetpwd
     */
    public function send()
    {
        $email = $this->request->post("email");
        $captcha = $this->request->post("captcha");
        $event = $this->request->post("event");
        $event = $event ? $event : 'register';

        //发送前验证码
        if (config('fastadmin.user_api_captcha')) {
            if (!\think\Validate::is($captcha, 'captcha')) {
                $this->error(__('Verification code is incorrect'));
            }
        }

        $last = Emslib::get($email, $event);
        if ($last && time() - $last['createtime'] < 60) {
            $this->error(__('Send frequently'));
        }
        if ($event) {
            $userinfo = User::getByEmail($email);
            if ($event == 'register' && $userinfo) {
                //已被注册
                $this->error(__('Already registered'));
            } elseif (in_array($event, ['changeemail']) && $userinfo) {
                //被占用
                $this->error(__('Already occupied'));
            } elseif (in_array($event, ['changepwd', 'resetpwd']) && !$userinfo) {
                //未注册
                $this->error(__('Not registered'));
            }
        }
        $ret = Emslib::send($email, null, $event);
        if ($ret) {
            $this->success(__('Send successful'));
        } else {
            $this->error(__('Send failed'));
        }
    }

    /**
     * 检测验证码
     *
     * @ApiMethod (POST)
     * @param string $email   邮箱
     * @param string $event   事件名称,register,changeemail,resetpwd
     * @param string $captcha 验证码
     */
    public function check()
    {
        $email = $this->request->post("email");
        $event = $this->request->post("event");
        $event = $event ? $event : 'register';
        $captcha = $this->request->post("captcha");

        if ($event) {
            $userinfo = User::getByEmail($email);
            if ($event == 'register' && $userinfo) {
                //已被注册
                $this->error(__('Already registered'));
            } elseif (in_array($event, ['changeemail']) && $userinfo) {
                //被占用
                $this->error(__('Already occupied'));
            } elseif (in_array($event, ['changepwd', 'resetpwd']) && !$userinfo) {
                //未注册
                $this->error(__('Not registered'));
            }
        }
        $ret = Emslib::check($email, $captcha, $event);
        if ($ret) {
            $this->success(__('Successful'));
        } else {
            $this->error(__('Verification code is incorrect'));
        }
    }
}
